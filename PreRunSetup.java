package frameworkSupport;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class PreRunSetup 
{
	public static void main(String[] args) 
	{
		deleteReportingDirectory();

	}
	
	public static void deleteReportingDirectory()
	{	
		
		String SRC_FOLDER = "./Reports/test-report";
		
		File directory = new File(SRC_FOLDER);
		 
    	//make sure directory exists
    	if(!directory.exists())
    	{
 
           System.out.println("Directory does not exist.");
           System.exit(0);
 
        }
    	else
    	{
 
           try
           {
        	   
               delete(directory);
        	
           }
           catch(IOException e1)
           {
               e1.printStackTrace();
               System.exit(0);
           }
        }
 
    	System.out.println("Existing Reporting Directory cleaned successfully.");
		
	}
	
	public static void delete(File file) throws IOException
	{
	 
	    	if(file.isDirectory())
	    	{
	 
	    		//directory is empty, then delete it
	    		if(file.list().length==0)
	    		{
	    			
	    		   file.delete();
	    		   System.out.println("Reporting Directory is deleted : " 
	                                                 + file.getAbsolutePath());
	    			
	    		}
	    		else
	    		{
	    			
	    		   //list all the directory contents
	        	   String files[] = file.list();
	     
	        	   for (String temp : files) 
	        	   {
	        	      //construct the file structure
	        	      File fileDelete = new File(file, temp);
	        		 
	        	      //recursive delete
	        	     delete(fileDelete);
	        	   }
	        		
	        	   //check the directory again, if empty then delete it
	        	   if(file.list().length==0)
	        	   {
	           	     file.delete();
	        	     System.out.println("Reporting Directory is deleted : " 
	                                                  + file.getAbsolutePath());
	        	   }
	    		}
	    		
	    	}
	    	else
	    	{
	    		//if file, then delete it
	    		file.delete();
	    		System.out.println("File is deleted : " + file.getAbsolutePath());
	    	}
	    }

}